const app = require('./src/app/index.js')
const { connect } = require('./src/db/mongoose.js')

connect('mongodb://localhost:27017/library')
  .then(connection => app.listen(3000, () => console.log('Serving in port 3000')))
  .catch(error => console.log(error))
