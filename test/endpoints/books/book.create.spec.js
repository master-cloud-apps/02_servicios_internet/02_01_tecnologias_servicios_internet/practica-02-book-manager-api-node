const request = require('supertest')
const app = require('./../../../src/app/index.js')
const { getIdFromResponseLocationHeader, manageInMemoryDatabase } = require('./../index.js')
const { expect } = require('chai')
const { bookTest } = require('../book.js')
const _ = require('lodash')

describe('Book use cases', () => {
  manageInMemoryDatabase()

  describe('Create book entity use cases', () => {
    it('When create a book should return 201 response', () => {
      return request(app)
        .post('/books')
        .send(bookTest)
        .then(response => expect(response.statusCode).to.equal(201))
    })
    it('When create book should return location with the recentlye created book id', () => {
      return request(app).post('/books').send(bookTest)
        .then(response => getIdFromResponseLocationHeader(response))
        .then(bookId => {
          expect(bookId).to.not.be.equal(undefined)
          expect(bookId).to.be.a('string')
        })
    })
    it('When create book no editorial should return bad request', () => {
      const noEditorialBook = _.cloneDeep(bookTest)
      delete noEditorialBook.editorial
      return request(app).post('/books').send(noEditorialBook)
        .then(response => {
          expect(response.statusCode).to.be.equal(400)
          expect(response.body.error).to.not.equal(undefined)
          expect(response.body.error.editorial.name).to.be.equal('ValidatorError')
        })
    })
    it('When create book no review should return bad request', () => {
      const noEditorialBook = _.cloneDeep(bookTest)
      delete noEditorialBook.editorial
      delete noEditorialBook.review
      return request(app).post('/books').send(noEditorialBook)
        .then(response => {
          expect(response.statusCode).to.be.equal(400)
          expect(response.body.error).to.not.equal(undefined)
          expect(response.body.error.editorial.name).to.be.equal('ValidatorError')
          expect(response.body.error.review.name).to.be.equal('ValidatorError')
        })
    })
  })
})
