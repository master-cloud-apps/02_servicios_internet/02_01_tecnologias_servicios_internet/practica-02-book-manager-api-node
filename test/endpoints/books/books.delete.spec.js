const request = require('supertest')
const app = require('./../../../src/app/index.js')
const { getIdFromResponseLocationHeader, manageInMemoryDatabase } = require('./../index.js')
const { expect } = require('chai')
const { createTestBook, deleteBookById, getBookById } = require('../book.js')

describe('Book use cases', () => {
  manageInMemoryDatabase()

  describe('DELETE /books/:bookId use cases', () => {
    let bookTestId
    beforeEach(() => createTestBook(app)
      .then(response => getIdFromResponseLocationHeader(response))
      .then(bookId => (bookTestId = bookId)))
    it('Given book created, when delete by id should return 204', () => {
      return request(app).delete(`/books/${bookTestId}`)
        .then(response => expect(response.statusCode).to.equal(204))
    })
    it('Given book created, when delete by id wrong id should return bad request', () => {
      return request(app).delete('/books/12345')
        .then(response => expect(response.statusCode).to.equal(400))
    })
    it('Given created book when delete book then get book by id should return 404', () => {
      return getBookById({ bookId: bookTestId, app })
        .then(response => expect(response.statusCode).to.be.equal(200))
        .then(_ => deleteBookById({ bookId: bookTestId, app }))
        .then(response => expect(response.statusCode).to.be.equal(204))
        .then(_ => getBookById({ bookId: bookTestId, app }))
        .then(response => expect(response.statusCode).to.be.equal(404))
    })
  })
})
