const { manageInMemoryDatabase, getIdFromResponseLocationHeader } = require('./../index.js')
const { expect } = require('chai')
const app = require('./../../../src/app/index.js')
const { createTestBook } = require('./../book.js')
const { createTestUserAndGetId, getUserById } = require('./../user.js')
const { createTestComment, getCommentById } = require('./../comment.js')
const mongoose = require('mongoose')

describe('Comments use cases', () => {
  manageInMemoryDatabase()
  describe('POST /books/:bookId/comments', () => {
    let testBookId
    let user
    beforeEach(() => createTestBook(app)
      .then(response => getIdFromResponseLocationHeader(response))
      .then(bookId => (testBookId = bookId))
      .then(() => createTestUserAndGetId(app))
      .then(userId => getUserById({ userId, app }))
      .then(response => (user = response.body)))
    describe('Given user and test created', () => {
      it('Given user and book created when create comment should return 201', () => {
        return createTestComment({ app, bookId: testBookId, nick: user.nick })
          .then(response => expect(response.statusCode).to.be.equal(201))
      })
      it('Given user and book created when create comment should return location', () => {
        return createTestComment({ app, bookId: testBookId, nick: user.nick })
          .then(response => expect(response.headers.location).to.be.a('string'))
      })
    })
    describe('GET /books/:bookId/comments/:commentId', () => {
      it('Given comment created, when get comment by id should return found', () => {
        return createTestComment({ app, bookId: testBookId, nick: user.nick })
          .then(response => getIdFromResponseLocationHeader(response))
          .then(commentId => getCommentById({ bookId: testBookId, commentId, app }))
          .then(response => expect(response.statusCode).to.be.equal(200))
      })
      it('Given comment created, when get comment by id should return comment', () => {
        return createTestComment({ app, bookId: testBookId, nick: user.nick })
          .then(response => getIdFromResponseLocationHeader(response))
          .then(commentId => getCommentById({ bookId: testBookId, commentId, app }))
          .then(response => expect(response.body.user.nick).to.be.equal('test_nick'))
      })
    })
  })
  describe('Given user created', () => {
    let user
    beforeEach(() => createTestUserAndGetId(app)
      .then(userId => getUserById({ userId, app }))
      .then(response => (user = response.body)))
    it('When create comment, should return 404, no book was found', () => {
      const notFoundBookId = new mongoose.Types.ObjectId()
      return createTestComment({ app, bookId: notFoundBookId, nick: user.nick })
        .then(response => expect(response.statusCode).to.be.equal(404))
    })
    it('When create comment, should return 400, bookId wrong', () => {
      return createTestComment({ app, bookId: 100, nick: user.nick })
        .then(response => expect(response.statusCode).to.be.equal(400))
    })
  })
  describe('Given book created', () => {
    let testBookId
    beforeEach(() => createTestBook(app)
      .then(response => getIdFromResponseLocationHeader(response))
      .then(bookId => (testBookId = bookId)))
    it('When create comment, should return 404, no user found', () => {
      return createTestComment({ app, bookId: testBookId, nick: 'user_not_found' })
        .then(response => expect(response.statusCode).to.be.equal(404))
    })
  })
})
