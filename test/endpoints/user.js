const { getIdFromResponseLocationHeader, createModel } = require('./index.js')
const request = require('supertest')

const testUser = {
  nick: 'test_nick',
  email: 'test_email@gmail.com'
}

const createTestUser = (app) => createModel({ model: testUser, modelType: 'user', app })

const createTestUserAndGetId = (app) =>
  createTestUser(app).then(response => getIdFromResponseLocationHeader(response))

const getUserById = ({ userId, app }) => request(app).get(`/users/${userId}`)

module.exports = {
  createTestUser, createTestUserAndGetId, testUser, getUserById
}
