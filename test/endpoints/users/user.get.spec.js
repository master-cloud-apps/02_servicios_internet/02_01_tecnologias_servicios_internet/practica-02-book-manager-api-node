const request = require('supertest')
const app = require('./../../../src/app/index.js')
const { manageInMemoryDatabase, getIdFromResponseLocationHeader } = require('./../index.js')
const { createTestUserAndGetId, getUserById } = require('./../user.js')
const { createTestBook } = require('./../book.js')
const { expect } = require('chai')
const { createTestComment } = require('../comment.js')

describe('User use cases', () => {
  manageInMemoryDatabase()
  describe('Get user by id use cases. Given user created', () => {
    it('When get user by id on existing user should return status code 200', () => {
      return createTestUserAndGetId(app)
        .then(userId => request(app).get(`/users/${userId}`))
        .then(response => expect(response.statusCode).to.equal(200))
    })
    it('When get user by id on existing user should return user', () => {
      return createTestUserAndGetId(app)
        .then(userId => request(app).get(`/users/${userId}`))
        .then(response => expect(response.body).to.deep.include({ nick: 'test_nick' }))
    })
    it('When get user by id wrong should return bad request', () => {
      return createTestUserAndGetId(app)
        .then(_ => request(app).get('/users/12345'))
        .then(response => expect(response.status).to.be.equal(400))
    })
    it('Given created user when delete user then get user by id should return 404', () => {
      let userTestId
      return createTestUserAndGetId(app)
        .then(userId => (userTestId = userId))
        .then(() => request(app).get(`/users/${userTestId}`))
        .then(response => expect(response.statusCode).to.be.equal(200))
        .then(() => request(app).delete(`/users/${userTestId}`))
        .then(response => expect(response.statusCode).to.be.equal(204))
        .then(() => request(app).get(`/users/${userTestId}`))
        .then(response => expect(response.statusCode).to.be.equal(404))
    })
  })
  describe('Get users use cases', () => {
    it('Given no user created, should return ok', () => {
      return request(app).get('/users')
        .then(response => expect(response.statusCode).to.be.equal(200))
    })
    it('Given no user created, should return empty list', () => {
      return request(app).get('/users')
        .then(response => expect(response.body.length).to.be.equal(0))
    })
    it('Given one user created, should return list size 1', () => {
      return createTestUserAndGetId(app)
        .then(() => request(app).get('/users'))
        .then(response => expect(response.body.length).to.be.equal(1))
    })
  })
  describe('Get user comments use cases', () => {
    let testBookId
    let user
    beforeEach(() => createTestBook(app)
      .then(response => getIdFromResponseLocationHeader(response))
      .then(bookId => (testBookId = bookId))
      .then(() => createTestUserAndGetId(app))
      .then(userId => getUserById({ userId, app }))
      .then(response => (user = response.body)))
    describe('Given usercreated', () => {
      it('Should get all comments from user', () => {
        return request(app).get(`/users/${user.id}/comments`)
          .then(response => {
            expect(response.statusCode).to.be.equal(200)
            expect(response.body.length).to.be.equal(0)
          })
      })
      it('When get comments form invalid user id should return bad request', () => {
        return request(app).get('/users/1/comments')
          .then(response => {
            expect(response.statusCode).to.be.equal(400)
          })
      })
    })

    describe('Given user and book created', () => {
      it('Shuld get all comments from user', () => {
        let commenTestId
        return createTestComment({ app, bookId: testBookId, nick: user.nick })
          .then(response => {
            commenTestId = getIdFromResponseLocationHeader(response)
            return request(app).get(`/users/${user.id}/comments`)
          })
          .then(response => {
            expect(response.statusCode).to.be.equal(200)
            expect(response.body.length).to.be.equal(1)
            expect(response.body[0].id).to.be.equal(commenTestId)
          })
      })
      it('Shuld get all comments from user (2 comments)', () => {
        const createCommentOverBookAndUser = () => createTestComment({ app, bookId: testBookId, nick: user.nick })
        return Promise.all([createCommentOverBookAndUser(), createCommentOverBookAndUser()])
          .then(() => request(app).get(`/users/${user.id}/comments`))
          .then(response => {
            expect(response.statusCode).to.be.equal(200)
            expect(response.body.length).to.be.equal(2)
          })
      })
    })
  })
})
