const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
  nick: { type: String, required: true, unique: true, dropDups: true },
  email: { type: String, required: true }
})

userSchema.virtual('comments', {
  ref: 'Comment',
  localField: '_id',
  foreignField: 'user'
})

userSchema.methods.toJSON = function() {
  const userObject = this.toObject()

  userObject.id = userObject._id

  delete userObject._id
  delete userObject.__v

  return userObject
}

module.exports = mongoose.model('User', userSchema)
