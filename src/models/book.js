const mongoose = require('mongoose')

const bookSchema = new mongoose.Schema({
  title: { type: String, required: true },
  author: { type: String, required: true },
  editorial: { type: String, required: true },
  yearPublication: { type: Number, required: true },
  review: { type: String, required: true }
})

bookSchema.virtual('comments', {
  ref: 'Comment',
  localField: '_id',
  foreignField: 'book'
})

bookSchema.methods.toJSON = function() {
  const bookObject = this.toObject()

  bookObject.id = bookObject._id

  delete bookObject._id
  delete bookObject.__v

  return bookObject
}

module.exports = mongoose.model('Book', bookSchema)
