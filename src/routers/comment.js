const express = require('express')
const { comments } = require('./../endpoints/index.js')

const router = new express.Router()
const commentHandler = comments()

router.post('/books/:bookId/comments', commentHandler.post)
router.get('/books/:bookId/comments/:commentId', commentHandler.getById)

module.exports = router
