const express = require('express')
const { users } = require('./../endpoints/index.js')

const router = new express.Router()
const userHandler = users()

router.post('/users', userHandler.post)
router.get('/users', userHandler.get)
router.get('/users/:userId/comments', userHandler.getCommentsByUserId)
router.get('/users/:userId', userHandler.getById)
router.put('/users/:userId', userHandler.updateById)
router.delete('/users/:userId', userHandler.deleteById)

module.exports = router
