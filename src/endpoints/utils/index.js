const url = require('url')
const mongoose = require('mongoose')

const getFullUrl = (req) => {
  const fullUrl = url.format({
    protocol: req.protocol,
    host: req.get('host'),
    pathname: req.originalUrl
  })

  return fullUrl + (fullUrl.endsWith('/') ? '' : '/')
}

const checkMongoId = (id, res) => {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return Promise.reject(res.status(400).send('Invalid id'))
  }
  return Promise.resolve(id)
}
const checkModelNone = (model, res) => {
  if (model === null || model === undefined) {
    return Promise.reject(res.sendStatus(404))
  }
  return Promise.resolve(model)
}

const logModel = model => {
  console.log(model)
  return model
}

const manageChainError = (error, response) => {
  if (error.statusCode === undefined) {
    return response.status(500).send(error)
  }
}

const createModel = (model, res) => {
  return model.save().catch(error => {
    if (error.errors) {
      return Promise.reject(res.status(400).send({ error: error.errors }))
    }
  })
}

module.exports = { getFullUrl, checkMongoId, checkModelNone, logModel, manageChainError, createModel }
