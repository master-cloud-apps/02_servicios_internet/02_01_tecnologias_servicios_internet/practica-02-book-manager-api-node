const { getFullUrl, checkModelNone, checkMongoId, manageChainError, createModel } = require('./../utils/index.js')
const Book = require('./../../models/book.js')
const _ = require('lodash')

const mapBookComments = (book, comments) => {
  return {
    ...book.toJSON(),
    comments: comments.map(comment => comment.toJSONNoFK())
  }
}

const handlers = () => ({
  get: (req, res) => {
    return Book.find().populate('comments')
      .then(books => res.status(200).send(books.map(book => mapBookComments(book, book.comments))))
      .catch(error => manageChainError(error, res))
  },
  post: (req, res) => {
    const createBookFromRequestBody = () => (new Book({
      title: req.body.title,
      author: req.body.author,
      editorial: req.body.editorial,
      yearPublication: req.body.yearPublication,
      review: req.body.review
    }))

    return Promise.resolve(createBookFromRequestBody())
      .then(book => createModel(book, res))
      .then(book => res.status(201).location(getFullUrl(req) + book.id).send())
      .catch(error => manageChainError(error, res))
  },
  getById: (req, res) => {
    const checkBookNone = book => checkModelNone(book, res)

    return Promise.resolve(checkMongoId(req.params.bookId, res))
      .then(id => Book.findById(id).populate('comments'))
      .then(book => checkBookNone(book))
      .then(book => book.populate({ path: 'comments' }).execPopulate())
      .then(bookPopulated => mapBookComments(bookPopulated, bookPopulated.comments))
      .then(mappedBook => res.status(200).send(mappedBook))
      .catch(error => manageChainError(error, res))
  },
  deleteById: (req, res) => {
    return Promise.resolve(checkMongoId(req.params.bookId, res))
      .then(id => Book.findById(id))
      .then(book => checkModelNone(book, res))
      .then(book => Book.findOneAndDelete({ _id: book._id }))
      .then(book => res.status(204).send(book.toJSON()))
      .catch(error => manageChainError(error, res))
  },
  updateById: (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['title', 'author', 'editorial', 'yearPublication', 'review']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
    const checkValidOperation = () => {
      if (!isValidOperation) {
        return res.status(400).send({ error: 'Invalid update field' })
      }
    }
    const mapBookForUpdate = book => {
      return updates.reduce((bookToUpdate, update) => {
        bookToUpdate[update] = req.body[update]
        return bookToUpdate
      }, _.cloneDeep(book))
    }

    return Promise.resolve(checkValidOperation())
      .then(() => checkMongoId(req.params.bookId, res))
      .then(id => Book.findById(id))
      .then(book => checkModelNone(book, res))
      .then(book => mapBookForUpdate(book))
      .then(updatedBook => updatedBook.save())
      .then(updatedBook => res.status(200).send(updatedBook.toJSON()))
      .catch(error => manageChainError(error, res))
  }
})

module.exports = handlers
