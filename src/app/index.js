const express = require('express')
const { userRouter, bookRouter, commentRouter } = require('./../routers/index.js')

const app = express()
app.use(express.json())
app.disable('x-powered-by')

app.use(userRouter)
app.use(bookRouter)
app.use(commentRouter)

module.exports = app
